<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitions', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique();
            $table->text('eligibility');
            $table->text('image_specifications');
            $table->text('judges');
            $table->text('awards');
            $table->text('disclaimer');
            $table->text('winners_notifications');
            $table->integer('position')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamp('start_date');
            $table->timestamp('close_date');
            $table->timestamp('announcement_date');
            $table->enum('participating', ['running', 'stop', 'pause', 'other'])->default('running');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitions');
    }
}
