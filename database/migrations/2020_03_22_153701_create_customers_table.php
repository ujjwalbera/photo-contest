<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('salutation', 20);
            $table->string('name', 100);
            $table->string('email', 100)->unique();
            $table->string('phone', 15)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('email_verify_token')->unique()->nullable();
            $table->timestamp('phone_verified_at')->nullable();
            $table->string('phone_verify_token')->unique()->nullable();
            $table->string('password', 150);
            $table->string('provider')->default('web');
            $table->string('provider_id')->default('0');
            $table->boolean('verified')->default(false);
            $table->boolean('status')->default(false);
            $table->string('profile_name', 100);
            $table->string('profile_picture', 255);
            $table->string('experience', 100);
            $table->date('dob');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
            $table->index(['email', 'phone', 'name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
