<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CamerasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cameras')->delete();
        $now = Carbon::now();
        DB::table('cameras')->insert([
            'name' => 'Apple',
            'slug' => 'apple',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Canon',
            'slug' => 'canon',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Fujifilm',
            'slug' => 'fujifilm',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'GoPro',
            'slug' => 'gopro',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Kodak',
            'slug' => 'kodak',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Lenovo',
            'slug' => 'lenovo',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Leica',
            'slug' => 'leica',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Nokia',
            'slug' => 'nokia',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Nikon',
            'slug' => 'nikon',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'OnePlus',
            'slug' => 'oneplus',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Olympus',
            'slug' => 'olympus',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Panasonic',
            'slug' => 'panasonic',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Pentax',
            'slug' => 'pentax',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Philips',
            'slug' => 'philips',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Samsung',
            'slug' => 'samsung',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Sony',
            'slug' => 'sony',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Vivo',
            'slug' => 'vivo',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Xiaomi',
            'slug' => 'xiaomi',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('cameras')->insert([
            'name' => 'Other',
            'slug' => '0ther',
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
