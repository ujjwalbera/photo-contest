<?php

use Illuminate\Database\Seeder;
//use DB;
use Carbon\Carbon;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        $now = Carbon::now();
        DB::table('categories')->insert([
                'id' => 1,
                'name' => 'Abstract',
                'slug' => 'abstract',
                'image' => 'assets/frontend/default/img/abstract.jpg',
                'color' => 'green',
                'position' => 1,
                'status' => true,
                'created_at' => $now,
                'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 2,
            'name' => 'Animal',
            'slug' => 'animal',
            'image' => 'assets/frontend/default/img/animal.jpg',
            'color' => 'white',
            'position' => 2,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 3,
            'name' => 'Black & White',
            'slug' => 'black-white',
            'image' => 'assets/frontend/default/img/black-white.jpg',
            'color' => 'orange',
            'position' => 3,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 4,
            'name' => 'Children',
            'slug' => 'children',
            'image' => 'assets/frontend/default/img/children.png',
            'color' => 'white',
            'position' => 4,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 5,
            'name' => 'Nature',
            'slug' => 'nature',
            'image' => 'assets/frontend/default/img/nature.jpg',
            'color' => 'blue',
            'position' => 5,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 6,
            'name' => 'Open',
            'slug' => 'open',
            'image' => 'assets/frontend/default/img/open.jpeg',
            'color' => 'white',
            'position' => 6,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 7,
            'name' => 'My Country',
            'slug' => 'my-country',
            'image' => 'assets/frontend/default/img/my-country.jpg',
            'color' => 'green',
            'position' => 7,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 8,
            'name' => 'People',
            'slug' => 'people',
            'image' => 'assets/frontend/default/img/people.jpg',
            'color' => 'white',
            'position' => 8,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 9,
            'name' => 'Portrait',
            'slug' => 'portrait',
            'image' => 'assets/frontend/default/img/portrait.jpg',
            'color' => 'red',
            'position' => 9,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 10,
            'name' => 'Sports',
            'slug' => 'sports',
            'image' => 'assets/frontend/default/img/sports.jpg',
            'color' => 'red',
            'position' => 10,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 11,
            'name' => 'Still Life',
            'slug' => 'still-life',
            'image' => 'assets/frontend/default/img/still-life.jpg',
            'color' => 'red',
            'position' => 11,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 12,
            'name' => 'Street',
            'slug' => 'street',
            'image' => 'assets/frontend/default/img/street.jpg',
            'color' => 'red',
            'position' => 12,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 13,
            'name' => 'Sunlight',
            'slug' => 'sunlight',
            'image' => 'assets/frontend/default/img/sunlight.jpeg',
            'color' => 'red',
            'position' => 13,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 14,
            'name' => 'Travel',
            'slug' => 'travel',
            'image' => 'assets/frontend/default/img/travel.jpg',
            'color' => 'red',
            'position' => 14,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 15,
            'name' => 'Weddings',
            'slug' => 'weddings',
            'image' => 'assets/frontend/default/img/weddings.jpg',
            'color' => 'red',
            'position' => 15,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('categories')->insert([
            'id' => 16,
            'name' => 'Other',
            'slug' => 'other',
            'image' => 'assets/frontend/default/img/other.jpg',
            'color' => 'red',
            'position' => 16,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
