<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class PackagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('packages')->delete();
        $now = Carbon::now();
        DB::table('packages')->insert([
            'id' => 1,
            'name' => 'Package Name 1',
            'slug' => 'package-name-1',
            'details' => 'The unique stripes of zebras make them one of the animals most familiar to people. They occur in a variety of habitats, such as grasslands, savannas, woodlands, thorny scrublands, mountains , and coastal hills. However, various anthropogenic factors have had a severe impact on zebra populations, in particular hunting for skins and habitat destruction. Grévy\'s zebra and the mountain highlighted text zebra are endangered.',
            'image' => 1,
            'price' => 3.00,
            'currency' => 'dollar',
            'position' => 1,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('packages')->insert([
            'id' => 2,
            'name' => 'Package Name 2',
            'slug' => 'package-name-2',
            'details' => 'The unique stripes of zebras make them one of the animals most familiar to people. They occur in a variety of habitats, such as grasslands, savannas, woodlands, thorny scrublands, mountains , and coastal hills. However, various anthropogenic factors have had a severe impact on zebra populations, in particular hunting for skins and habitat destruction. Grévy\'s zebra and the mountain highlighted text zebra are endangered.',
            'image' => 3,
            'price' => 8.00,
            'currency' => 'dollar',
            'position' => 2,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('packages')->insert([
            'id' => 3,
            'name' => 'Package Name 3',
            'slug' => 'package-name-3',
            'details' => 'The unique stripes of zebras make them one of the animals most familiar to people. They occur in a variety of habitats, such as grasslands, savannas, woodlands, thorny scrublands, mountains , and coastal hills. However, various anthropogenic factors have had a severe impact on zebra populations, in particular hunting for skins and habitat destruction. Grévy\'s zebra and the mountain highlighted text zebra are endangered.',
            'image' => 5,
            'price' => 13.00,
            'currency' => 'dollar',
            'position' => 3,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('packages')->insert([
            'id' => 4,
            'name' => 'Package Name 4',
            'slug' => 'package-name-4',
            'details' => 'The unique stripes of zebras make them one of the animals most familiar to people. They occur in a variety of habitats, such as grasslands, savannas, woodlands, thorny scrublands, mountains , and coastal hills. However, various anthropogenic factors have had a severe impact on zebra populations, in particular hunting for skins and habitat destruction. Grévy\'s zebra and the mountain highlighted text zebra are endangered.',
            'image' => 10,
            'price' => 25.00,
            'currency' => 'dollar',
            'position' => 4,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('packages')->insert([
            'id' => 5,
            'name' => 'Package Name 5',
            'slug' => 'package-name-5',
            'details' => 'The unique stripes of zebras make them one of the animals most familiar to people. They occur in a variety of habitats, such as grasslands, savannas, woodlands, thorny scrublands, mountains , and coastal hills. However, various anthropogenic factors have had a severe impact on zebra populations, in particular hunting for skins and habitat destruction. Grévy\'s zebra and the mountain highlighted text zebra are endangered.',
            'image' => 20,
            'price' => 48.00,
            'currency' => 'dollar',
            'position' => 5,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('packages')->insert([
            'id' => 6,
            'name' => 'Package Name 6',
            'slug' => 'package-name-6',
            'details' => 'The unique stripes of zebras make them one of the animals most familiar to people. They occur in a variety of habitats, such as grasslands, savannas, woodlands, thorny scrublands, mountains , and coastal hills. However, various anthropogenic factors have had a severe impact on zebra populations, in particular hunting for skins and habitat destruction. Grévy\'s zebra and the mountain highlighted text zebra are endangered.',
            'image' => 30,
            'price' => 68.00,
            'currency' => 'dollar',
            'position' => 6,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
