<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class CompetitionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('competitions')->delete();
        $now = Carbon::now();
        $close_date = $now->addDays(10);
        $announcement_date = $now->addDays(15);
        DB::table('competitions')->insert([
            'id' => 1,
            'name' => 'The Competition 1',
            'slug' => 'the-competition-1',
            'eligibility' => "<p>This contest is open to all amateur photographers in the United States, Canada, and around the world.<p>",
            'image_specifications' => "<p>Image Size must have a long dimension of at least 1000 pixels and less than 2000 pixels</p><p>Save file as (JPG) with maximum quality.</p><p>File Size should be no more than 4 Mb</p><p>Do not upload photos with watermark names, image frames or borders.</p>",
            'judges' => "<p>Experts from across the photographic industry to determine the Winners and Honorable Mentions, we employ a panel of three judges. Our judges are always professional photographers who are also college-level photography instructors.</p>",
            'awards' => "<p>Grand Prize Winner, Photographer of the Year will receive $500 USD Cash Prize</p><p>1st Place Winner, Photographer of the Year will receive $300 USD Cash Prize</p><p>2nd Place Winner, Photographer of the Year will receive $200 USD Cash Prize</p><p>3rd Place Winner, Photographer of the Year will receive $100 USD Cash Prize</p><p>16 Golden Awards  – The Photograph's Golden Awards will win highest aggregate scores in a competition.</p><p>16 Silver Awards – The Photograph's Silver Awards will win second highest aggregate scores in a competition.</p><p>16 Bronze Awards – The Photograph's Bronze Awards will win third highest aggregate scores in a competition.</p><p>Honourable Mentions – The Photograph's Honourable Mention are awarded for the 4th and 5th highest aggregate scores in a competition.</p><p>Certificates - All Winners and Honorable Mentions from each categories will receive winners certificate.</p>",
            'disclaimer' => "<p>Access to and use of thephotograph.org (the \"Site\") is provided by Apawards Media LLP (\"The Photograph Org\") to you (\"You\"/\"Your\"), subject to the following terms and conditions (\"Terms\").</p><p>In consideration of the ThePhotograph allowing you to access the Site, you are deemed to have accepted the Terms, which shall take effect immediately on your first use of the Site, whether or not you register as a user or enter the Competition. If you do not agree to be legally bound by all the following Terms please do not access and/or use the Site. We reserve the right, to modify this Privacy Statement at any time.</p>",
            'winners_notifications' => "<p>Winners & Honorable mention will be published on the website and contacted via email after November.</p><p>GOT MORE QUESTIONS? Most answers and solutions can be found in our FAQ</p>",
            'start_date' => $now,
            'close_date' => $close_date,
            'announcement_date' => $announcement_date,
            'position' => 1,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
