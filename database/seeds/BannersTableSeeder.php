<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BannersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->delete();
        $now = Carbon::now();
        DB::table('banners')->insert([
            'id' => 1,
            'title' => 'Banner 1',
            'desc' => 'Banner 1 desc',
            'placeholder' => 'home_banner',
            'image' => 'assets/frontend/default/img/home01_slider1.jpg',
            'link' => '#',
            'position' => 1,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('banners')->insert([
            'id' => 2,
            'title' => 'Banner 2',
            'desc' => 'Banner 2 desc',
            'placeholder' => 'home_banner',
            'image' => 'assets/frontend/default/img/home01_slider2.jpg',
            'link' => '#',
            'position' => 2,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('banners')->insert([
            'id' => 3,
            'title' => 'Banner 3',
            'desc' => 'Banner 3 desc',
            'placeholder' => 'home_banner',
            'image' => 'assets/frontend/default/img/home01_slider3.jpg',
            'link' => '#',
            'position' => 3,
            'status' => true,
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
