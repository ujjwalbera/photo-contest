<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->delete();
        $now = Carbon::now();
        DB::table('admins')->insert([
            'id' => 1,
            'name' => 'Ujjwal Bera',
            'phone' => '7319473777',
            'email' => 'ujjwal.test@gmail.com',
            'password' => '$2y$10$EokTlFWfIg1FSGZSthJvUe59ShRd67ZTN/ZJo.Ocwj6VQ7b5VuRoC',
            'status' => true,
            'remember_token' => 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt',
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
