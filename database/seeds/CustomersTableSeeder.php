<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->delete();
        $now = Carbon::now();
        DB::table('customers')->insert([
                'id' => 1,
                'salutation' => 'Mr.',
                'name' => 'Ujjwal Bera',
                'phone' => '7319473777',
                'email' => 'ujjwal.bera@gmail.com',
                'email_verified_at' => $now,
                'email_verify_token' => 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt',
                'phone_verified_at' => $now,
                'phone_verify_token' => 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt',
                'password' => '$2y$10$EokTlFWfIg1FSGZSthJvUe59ShRd67ZTN/ZJo.Ocwj6VQ7b5VuRoC',
                'verified' => true,
                'status' => true,
                'remember_token' => 'OMIFmypRP603nk1RbgayNX1pANnjsb2oI74ZGObt',
                'profile_name' => 'Ujjwal Bera',
                'profile_picture' => 'images/profiles/customers/ujjwalbera.jpeg',
                'experience' => 'Amateur Photographer',
                'dob' => '1986-03-15',
                'created_at' => $now,
                'updated_at' => $now
        ]);
    }
}
