@extends('layouts.admin.dashboard')
@section('pageTitle', 'In-active Package Listing')
@section('content')
    <div class="col-sm-12">
        <!-- Zero config.table start -->
        <div class="card">
            <div class="card-header">
                <h5>Entry Fee Listings</h5>
                <span>Showing all in-active entry fee packages</span>

            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Details</th>
                            <th>Currency</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Created On</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($packages) > 0)
                            @foreach($packages as $package)
                            <tr>
                                <td>{{ $package->name }}</td>
                                <td>{{ substr($package->details,0,30).'...' }}</td>
                                <td>{{ $package->currency_code }}</td>
                                <td><i class="fa fa-{{ $package->currency }}"></i> {{ $package->price }}</td>
                                <td>{{ $package->status }}</td>
                                <td>{{ $package->created_at }}</td>
                                <td>{{ $package->status }}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7">No in-active entry fee packages found!</td>
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Details</th>
                            <th>Currency</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Created On</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- Zero config.table end -->
    </div>
@endsection
