@extends('layouts.admin.dashboard')
@section('pageTitle', 'In-competition Listing')
@section('content')
    <div class="col-sm-12">
        <!-- Zero config.table start -->
        <div class="card">
            <div class="card-header">
                <h5>Competition Listings</h5>
                <span>Showing all in-active competitions</span>

            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Eligibility</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>status</th>
                            <th>Created On</th>
                            <th>Competition</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($competitions) > 0)
                            @foreach($competitions as $competition)
                                <tr>
                                    <td>{{ $competition->name }}</td>
                                    <td>{{ substr(strip_tags($competition->eligibility),0,30).'...' }}</td>
                                    <td>{{ date( 'd/m/Y', strtotime($competition->start_date)) }}</td>
                                    <td>{{ date( 'd/m/Y', strtotime($competition->close_date)) }}</td>
                                    <td>{{ $competition->status }}</td>
                                    <td>{{ date( 'd/m/Y', strtotime($competition->created_at)) }}</td>
                                    <td>{{ $competition->participating }}</td>
                                    <td>{{ $competition->participating }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">No in-active competition found!</td>
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Eligibility</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>status</th>
                            <th>Created On</th>
                            <th>Competition</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- Zero config.table end -->
    </div>
@endsection
