@extends('layouts.admin.dashboard')
@section('pageTitle', 'Competition Participate Listings')
@section('content')
    <div class="col-sm-12">
        <!-- Zero config.table start -->
        <div class="card">
            <div class="card-header">
                <h5>Competition Participate Listings</h5>
                <span>Showing all competition participate</span>

            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="simpletable" class="table table-striped table-bordered nowrap">
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Camera</th>
                            <th>status</th>
                            <th>Created On</th>
                            <th>Competition</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($participates) > 0)
                            @foreach($participates as $participate)
                            <tr>
                                <td>{{ $participate->customer->name }}</td>
                                <td>{{ $participate->title }}</td>
                                <td>{{ $participate->category->name }}</td>
                                <td>{{ $participate->camera->name }}</td>
                                <td>{{ $participate->status }}</td>
                                <td>{{ date( 'd/m/Y', strtotime($participate->created_at)) }}</td>
                                <td>{{ $participate->competition->name }}</td>
                                <td>{{ $participate->competition->participating }}</td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="8">No active competition participates found!</td>
                        @endif
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Customer Name</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Camera</th>
                            <th>status</th>
                            <th>Created On</th>
                            <th>Competition</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- Zero config.table end -->
    </div>
@endsection
