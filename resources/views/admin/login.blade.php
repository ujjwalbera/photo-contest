@extends('layouts.admin.login')
@section('pageTitle', 'Admin Login')
@section('content')
    <form class="md-float-material form-material" action="{{ route('admin-login') }}" method="post">
        @csrf
        <div class="text-center">
            <img src="{!! URL::asset('assets/admin/adminty/images/logo.png') !!}" alt="logo.png">
        </div>
        <div class="auth-box card">
            <div class="card-block">
                <div class="row m-b-20">
                    <div class="col-md-12">
                        <h3 class="text-center">Sign In</h3>
                    </div>
                </div>
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
                <div class="form-group form-primary">
                    <input type="text" name="username" class="form-control" placeholder="Username">
                    @error('username') <span class="form-bar">{{ $message }}</span>@enderror
                </div>
                <div class="form-group form-primary">
                    <input type="password" name="password" class="form-control" placeholder="Password">
                    @error('password') <span class="form-bar">{{ $message }}</span>@enderror
                </div>
                <div class="row m-t-25 text-left">
                    <div class="col-12">
                        <div class="checkbox-fade fade-in-primary d-">
                            <label>
                                <input type="checkbox" value="">
                                <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                <span class="text-inverse">Remember me</span>
                            </label>
                        </div>
                        <div class="forgot-phone text-right f-right">
                            <a href="auth-reset-password.htm" class="text-right f-w-600"> Forgot Password?</a>
                        </div>
                    </div>
                </div>
                <div class="row m-t-30">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Sign in</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
