<aside class="menu-sidebar d-none d-lg-block">
    <div class="logo">
        <a href="#">
            <img src="{!! URL::asset('assets/customer/images/icon/logo.png') !!}" alt="Cool Admin" />
        </a>
    </div>
    <div class="menu-sidebar__content js-scrollbar1">
        <nav class="navbar-sidebar">
            <ul class="list-unstyled navbar__list">
                <li class="@if(Route::currentRouteName() == 'customer-dashboard') active @endif">
                    <a class="js-arrow" href="{{route('customer-dashboard')}}">
                        <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                </li>
                <li class="has-sub @if(Route::currentRouteName() == 'customer-package' || Route::currentRouteName() == 'customer-package-buy') active @endif">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-th-list"></i>Entry Fees</a>
                        <ul class="list-unstyled navbar__sub-list js-sub-list">
                            <li>
                                <a href="{{route('customer-package')}}"><i class="fas fa-th-list"></i>Entry Fee Lists</a>
                            </li>
                            <li>
                                <a href="{{route('customer-package-buy')}}"><i class="fas fa-dollar"></i>Buy Entry Pass</a>
                            </li>
                        </ul>
                </li>
                <li class="has-sub @if(Route::currentRouteName() == 'customer-competition-participate-lists' || Route::currentRouteName() == 'customer-competition-participate') active @endif">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-camera"></i>Competitions</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{route('customer-competition-participate-lists')}}"><i class="fas fa-list"></i>Competitions</a>
                        </li>
                        <li>
                            <a href="{{route('customer-competition-participate')}}"><i class="fas fa-camera"></i>Submit Photo</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub @if(Route::currentRouteName() == 'customer-profile' || Route::currentRouteName() == 'customer-biography' ||Route::currentRouteName() == 'customer-change-password') active @endif">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-user"></i>Profile</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list">
                        <li>
                            <a href="{{route('customer-profile')}}"><i class="fas fa-user"></i>Update Profile</a>
                        </li>
                        <li>
                            <a href="{{route('customer-biography')}}"><i class="fas fa-file"></i>Update Biography</a>
                        </li>
                        <li>
                            <a href="{{route('customer-change-password')}}"><i class="fas fa-barcode"></i>Change Password</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('customer-dologout')}}"><i class="zmdi zmdi-power"></i>Logout</a>
                </li>
            </ul>
        </nav>
    </div>
</aside>
