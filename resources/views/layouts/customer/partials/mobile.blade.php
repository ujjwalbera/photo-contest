<header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="index.html">
                    <img src="{!! URL::asset('assets/customer/images/icon/logo.png') !!}" alt="CoolAdmin" />
                </a>
                <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
                <li class="@if(Route::currentRouteName() == 'customer-dashboard') active @endif">
                    <a class="js-arrow" href="{{route('customer-dashboard')}}">
                        <i class="fas fa-tachometer-alt"></i>Dashboard</a>
                </li>
                <li class="has-sub @if(Route::currentRouteName() == 'customer-package' || Route::currentRouteName() == 'customer-package-buy')
                    active @endif">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-th-list"></i>Entry Fees</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="{{route('customer-package')}}"><i class="fas fa-th-list"></i>Entry Fee Lists</a>
                        </li>
                        <li>
                            <a href="{{route('customer-package-buy')}}"><i class="fas fa-dollar"></i>Buy Entry Pass</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub @if(Route::currentRouteName() == 'customer-competition-participate-lists' || Route::currentRouteName() == 'customer-competition-participate') active @endif">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-camera"></i>Competitions</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="{{route('customer-competition-participate-lists')}}"><i class="fas fa-list"></i>Competitions</a>
                        </li>
                        <li>
                            <a href="{{route('customer-competition-participate')}}"><i class="fas fa-camera"></i>Submit Photo</a>
                        </li>
                    </ul>
                </li>
                <li class="has-sub  @if(Route::currentRouteName() == 'customer-profile' || Route::currentRouteName() == 'customer-biography' ||Route::currentRouteName() == 'customer-change-password') active @endif">
                    <a class="js-arrow" href="#">
                        <i class="fas fa-user"></i>Profile</a>
                    <ul class="navbar-mobile-sub__list list-unstyled js-sub-list">
                        <li>
                            <a href="{{route('customer-profile')}}"><i class="fas fa-user"></i>Update Profile</a>
                        </li>
                        <li>
                            <a href="{{route('customer-biography')}}"><i class="fas fa-file"></i>Update Biography</a>
                        </li>
                        <li>
                            <a href="{{route('customer-change-password')}}"><i class="fas fa-barcode"></i>Change Password</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('customer-dologout')}}"><i class="zmdi zmdi-power"></i>Logout</a>
                </li>
            </ul>
        </div>
    </nav>
</header>
