﻿<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>@yield('pageTitle') :: Photo Contest</title>

    <!-- Fontfaces CSS-->
    <link href="{!! URL::asset('assets/customer/css/font-face.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/font-awesome-4.7/css/font-awesome.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/font-awesome-5/css/fontawesome-all.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/mdi-font/css/material-design-iconic-font.min.css') !!}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="{!! URL::asset('assets/customer/vendor/bootstrap-4.1/bootstrap.min.css') !!}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{!! URL::asset('assets/customer/vendor/animsition/animsition.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/wow/animate.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/css-hamburgers/hamburgers.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/slick/slick.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/select2/select2.min.css') !!}" rel="stylesheet" media="all">
    <link href="{!! URL::asset('assets/customer/vendor/perfect-scrollbar/perfect-scrollbar.css') !!}" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="{!! URL::asset('assets/customer/css/theme.css') !!}" rel="stylesheet" media="all">

    <link href="{!! URL::asset('assets/customer/css/editor.css') !!}" rel="stylesheet" media="all">

    <!-- Jquery JS-->
    <script src="{!! URL::asset('assets/customer/vendor/jquery-3.2.1.min.js') !!}"></script>
    <!-- Bootstrap JS-->
    <script src="{!! URL::asset('assets/customer/vendor/bootstrap-4.1/popper.min.js') !!}"></script>
    <script src="{!! URL::asset('assets/customer/vendor/bootstrap-4.1/bootstrap.min.js') !!}"></script>
    <script src="{!! URL::asset('assets/customer/js/editor.js') !!}"></script>
</head>

<body class="animsition">
<div class="page-wrapper">
    <!-- HEADER MOBILE-->
    @include('layouts.customer.partials.mobile')
    <!-- END HEADER MOBILE-->

    <!-- MENU SIDEBAR-->
    @include('layouts.customer.partials.sidebar')
    <!-- END MENU SIDEBAR-->

    <!-- PAGE CONTAINER-->
    <div class="page-container">
        <!-- HEADER DESKTOP-->
        @include('layouts.customer.partials.header')
        <!-- END HEADER DESKTOP-->

        <!-- MAIN CONTENT-->
        <div class="main-content">
            <div class="section__content section__content--p30">
                <div class="container-fluid">
                    <div class="row">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END PAGE CONTAINER-->

</div>


<!-- Vendor JS       -->
<script src="{!! URL::asset('assets/customer/vendor/slick/slick.min.js') !!}">
</script>
<script src="{!! URL::asset('assets/customer/vendor/wow/wow.min.js') !!}"></script>
<script src="{!! URL::asset('assets/customer/vendor/animsition/animsition.min.js') !!}"></script>
<script src="{!! URL::asset('assets/customer/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') !!}">
</script>
<script src="{!! URL::asset('assets/customer/vendor/counter-up/jquery.waypoints.min.js') !!}"></script>
<script src="{!! URL::asset('assets/customer/vendor/counter-up/jquery.counterup.min.js') !!}">
</script>
<script src="{!! URL::asset('assets/customer/vendor/circle-progress/circle-progress.min.js') !!}"></script>
<script src="{!! URL::asset('assets/customer/vendor/perfect-scrollbar/perfect-scrollbar.js') !!}"></script>
<script src="{!! URL::asset('assets/customer/vendor/chartjs/Chart.bundle.min.js') !!}"></script>
<script src="{!! URL::asset('assets/customer/vendor/select2/select2.min.js') !!}">
</script>

<!-- Main JS-->
<script src="{!! URL::asset('assets/customer/js/main.js') !!}"></script>





</body>

</html>
<!-- end document-->
