<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}"class="liza_content_under_header liza_transparent_header">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <link rel="icon" href="{!! URL::asset('assets/frontend/default/img/pm-logo192.png') !!}" sizes="192x192" />
    <link rel="apple-touch-icon-precomposed" href="img/pm-logo192.png" />
    <title>Photo Contest</title>
    <meta name="keywords" content="HTML5 Template">
    <meta name="description" content="Responsive HTML5 Template">

    <!--Mobile Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!--Web Fonts-->
    <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>

    <!-- CSS-->
    <link rel="stylesheet" id="fontawesome-css" href="{!! URL::asset('assets/frontend/default/css/font-awesome.min.css') !!}" type="text/css" media="all" />
    <link rel="stylesheet" id="kube-css" href="{!! URL::asset('assets/frontend/default/css/kube.css') !!}" type="text/css" media="all" />
    <link rel="stylesheet" id="swipebox-css" href="{!! URL::asset('assets/frontend/default/css/swipebox.css') !!}" type="text/css" media="all" />
    <link rel="stylesheet" id="owl-css" href="{!! URL::asset('assets/frontend/default/css/owl.carousel.css') !!}" type="text/css" media="all" />

    <link rel="stylesheet" id="liza-modules-css" href="{!! URL::asset('assets/frontend/default/css/modules.css') !!}" type="text/css" media="all" />
    <link rel="stylesheet" id="liza-theme-css" href="{!! URL::asset('assets/frontend/default/css/theme.css') !!}" type="text/css" media="all" />
    <link rel="stylesheet" id="liza-responsive-css" href="{!! URL::asset('assets/frontend/default/css/responsive.css') !!}" type="text/css" media="all" />
    <link rel="stylesheet" id="liza-settings-css" href="{!! URL::asset('assets/frontend/default/css/settings.css') !!}" type="text/css" media="all" />
</head>
<body>
    @include('layouts.users.partials.header')
    <div class="liza_container liza_blur_on_aside liza_no_sidebar liza_header_padding_no liza_footer_padding_yes">
        <div class="liza_content_no_sidebar liza_content">
            <div class="liza_tiny">
                @include('layouts.users.partials.banner')
                @include('layouts.users.partials.catbox')
                @yield('content')
            </div><!-- .liza_tiny -->
        </div><!-- .liza_content -->
    </div><!-- .liza_container -->
    @include('layouts.users.partials.footer')
    <div class="liza_back_to_top liza_show_me" data-bottom="50"></div>

<!-- Scripts -->
<script src="{!! URL::asset('assets/frontend/default/js/jquery.min.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/kube.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/jquery.mousewheel.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/jquery.swipebox.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/jquery.waypoints.min.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/owl.carousel.min.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/jquery.isotope.min.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/jquery.event.swipe.js') !!}"></script>

<script src="{!! URL::asset('assets/frontend/default/js/simple_slider.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/pm_counter.js') !!}"></script>
<script src="{!! URL::asset('assets/frontend/default/js/posts_sorting.js') !!}"></script>

<script src="{!! URL::asset('assets/frontend/default/js/theme.js') !!}"></script>
</body>
</html>
