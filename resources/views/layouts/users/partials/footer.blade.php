<footer class="liza_footer liza_blur_on_aside logo_center">
    <div class="liza_footer_left_part">
        <div class="footer_email"><a href="mailto:liza@pixel-mafia.com">liza@pixel-mafia.com</a></div>
        <div class="footer_copyright">&copy; Copyright 2017 Liza Henderson. All Rights Reserved.</div>
    </div>
    <div class="liza_footer_middle_part">
        <div class="liza_logo_cont">
            <div class="liza_logo_wrapper liza_blur_on_aside"><a href="index.html" class="liza_image_logo liza_retina"><img src="img/logo.png" alt="" width="145" height="59" /></a></div>
        </div>
    </div>
    <div class="liza_footer_right_part">
        <div class="footer_email">252 Edison St. Salt Lake City, Utah</div>
        <div class="footer_copyright">801 467 2207</div>
    </div>
    <div class="clear"></div>
</footer>
