<div class="row liza_js_padding" data-padBottom="99">
    @foreach($categories as $category)
        <div class="col col-4 liza_js_padding" data-padRight="0" data-padRightTablet="0" data-padBottomTablet="0" @if(($category->position % 2) != 0)
        style="background-color: {{$category->color}};" @else style="background-image: url('{!! URL::asset($category->image) !!}'); background-size: cover;" @endif>
            <div class="cat_box_inner_color">
                <h3>{{$category->name}}</h3>
            </div>
        </div><!-- .col col-4 -->
    @endforeach

    <!--<div class="col col-4 liza_js_padding cat_box" data-padLeft="0" data-padLeftTablet="0" style="background-image: url('{!! URL::asset('assets/frontend/default/img/home01.jpg') !!}');">
        <div class="cat_box_inner_color">
            <h3>Category 2</h3>
        </div>
    </div>--><!-- .col col-4 -->
        <!--<div class="col col-4 liza_js_padding cat_box" data-padRight="0" data-padRightTablet="0" data-padBottomTablet="0" style="background-color: green;">
            <div class="cat_box_inner_color">
                <h3>Category 3</h3>
            </div>
        </div>--><!-- .col col-4 -->

    <!--<div class="col col-4 liza_js_padding cat_box" data-padLeft="0" data-padLeftTablet="0" style="background-image: url('{!! URL::asset('assets/frontend/default/img/home01.jpg') !!}');">
        <div class="cat_box_inner_color">
            <h3>Category 4</h3>
        </div>
    </div>--><!-- .col col-4 -->
        <!--<div class="col col-4 liza_js_padding cat_box" data-padRight="0" data-padRightTablet="0" data-padBottomTablet="0" style="background-color: green;">
            <div class="cat_box_inner_color">
                <h3>Category 5</h3>
            </div>
        </div>--><!-- .col col-4 -->

    <!--<div class="col col-4 liza_js_padding cat_box" data-padLeft="0" data-padLeftTablet="0" style="background-image: url('{!! URL::asset('assets/frontend/default/img/home01.jpg') !!}');">
        <div class="cat_box_inner_color">
            <h3>Category 6</h3>
        </div>
    </div>--><!-- .col col-4 -->
</div><!-- .row -->
