<div class="row liza_set_fullwidth liza_js_padding" data-padBottom="122">
    <div class="col col-12">
        <div class="liza_simple_slider_wrapper no_fit  content_left autoplay_on presetup custom_content" data-uniqid = "9504">
            <div class="liza_simple_slider liza_slider_9504"
                 data-height = "100%"
                 data-uniqid = "9504"
                 data-content = "custom_content"
                 data-autoplay = "on"
                 data-interval = "5000">
                @foreach($banners as $banner)
                <div class="liza_simple_slide liza_js_bg_color liza_js_bg_image simple_slide_loader liza_simple_slide{{$banner->position}}"
                     data-title="{{$banner->title}}"
                     data-src="{!! URL::asset($banner->image) !!}"
                     data-bgcolor = "#0b0c11"
                     data-count="{{$banner->position}}"></div>
                @endforeach
                <!--<div class="liza_simple_slide liza_js_bg_color liza_js_bg_image simple_slide_loader  liza_simple_slide2"
                     data-title="Smoking time"
                     data-src="{!! URL::asset('assets/frontend/default/img/home01_slider2.jpg') !!}"
                     data-bgcolor = "#0b0c11"
                     data-count="2"></div>

                <div class="liza_simple_slide liza_js_bg_color liza_js_bg_image simple_slide_loader  liza_simple_slide3"
                     data-title="Mirroring"
                     data-src="{!! URL::asset('assets/frontend/default/img/home01_slider3.jpg') !!}"
                     data-bgcolor = "#0b0c11"
                     data-count="3"></div>-->
            </div>
            <div class="liza_simple_slider_overlay liza_js_bg_color" data-bgcolor="rgba(0,0,0,0.2)"></div>
            <div class="liza_simple_slider_content">
                <h1 class="liza_simple_slider_title liza_js_color" data-color="#ffffff">The Sweetest and Darkest Side</h1>
                <div class="liza_simple_slider_button_wrapper">
                    <a href="albums-slider.html" class="liza_button">view more</a>
                </div>
            </div>

            <div class="liza_simple_slider_controls">
                <a href="javascript:void(0)" class="liza_button_prev liza_simple_slider_prev"></a>
                <a href="javascript:void(0)" class="liza_button_next liza_simple_slider_next"></a>
                <a href="javascript:void(0)" class="liza_button_fullview liza_simple_slider_fullview"></a>
            </div>
        </div><!-- .liza_simple_slider_wrapper -->
    </div>
</div><!-- .row -->
