<!-- Aside Menu -->
<div class="liza_aside_menu_wrapper">
    <a class="liza_menu_toggler" href="javascript:void(0)">
			<span class="liza_menu_ico">
				<span class="liza_menu_ico_line1"></span>
				<span class="liza_menu_ico_line2"></span>
				<span class="liza_menu_ico_line3"></span>
			</span>
    </a>

    <nav class="liza_main_nav">
        <ul class="liza_menu">
            <li><a href="{{route('home-index')}}">Home</a></li>
            <li class="menu-item-has-children"><a href="javascript:void(0)">Pages</a>
                <ul class="sub-menu">
                    <li><a href="about.html">About</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="404.html">404 Error Page</a></li>
                    <li><a href="coming_soon.html">Coming Soon</a></li>
                </ul>
            </li>
            <li class="menu-item-has-children"><a href="javascript:void(0)">Galleries</a>
                <ul class="sub-menu">
                    <li><a href="albums-grid.html">Albums Grid</a></li>
                    <li><a href="albums-slider.html">Albums Slider</a></li>
                    <li><a href="albums-content-slider.html">Albums Content Slider</a></li>
                    <li class="menu-item-has-children"><a href="javascript:void(0)">Albums Сontent</a>
                        <ul class="sub-menu">
                            <li><a href="album_single-portrait.html">Portrait Slider</a></li>
                            <li><a href="album_single-masonry.html">Masonry Album</a></li>
                            <li><a href="album_single-grid.html">Grid Album</a></li>
                            <li><a href="album_single-ribbon.html">Ribbon Album</a></li>
                            <li><a href="album_single-ribbons.html">Two Ribbons Album</a></li>
                            <li><a href="album_single-slider.html">Slider Album</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="menu-item-has-children"><a href="javascript:void(0)">Projects</a>
                <ul class="sub-menu">
                    <li><a href="projects-listing.html">Projects Listing</a></li>
                    <li><a href="projects-grid.html">Projects Grid</a></li>
                    <li><a href="projects-masonry.html">Projects Masonry</a></li>
                    <li class="menu-item-has-children"><a href="javascript:void(0)">Project Page</a>
                        <ul class="sub-menu">
                            <li><a href="project_single-fullwidth.html">Fullwidth</a></li>
                            <li><a href="project_single-right.html">Right Sidebar</a></li>
                            <li><a href="project_single-left.html">Left Sidebar</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="menu-item-has-children"><a href="javascript:void(0)">Blog</a>
                <ul class="sub-menu">
                    <li class="menu-item-has-children"><a href="javascript:void(0)">Blog Posts</a>
                        <ul class="sub-menu">
                            <li><a href="blog_single-standart.html">Standart Post</a></li>
                            <li><a href="blog_single-image.html">Image Post</a></li>
                            <li><a href="blog_single-quote.html">Quote Post</a></li>
                            <li><a href="blog_single-audio.html">Audio Post</a></li>
                            <li><a href="blog_single-video.html">Video Post</a></li>
                        </ul>
                    </li>
                    <li><a href="blog-masonry.html">Masonry Listing</a></li>
                    <li><a href="blog-fullwidth.html">Fullwidth Listing</a></li>
                    <li><a href="blog-right-sidebar.html">Right Sidebar</a></li>
                    <li><a href="blog-left-sidebar.html">Left Sidebar</a></li>
                </ul>
            </li>
            <li><a href="contacts.html">Contacts</a></li>
        </ul>
    </nav>

    <div class="liza_aside_menu_share">
        <span class="liza_aside_menu_share_label">Share</span>
        <a target="_blank"
           href="https://www.facebook.com/share.html?u=http://pixel-mafia.com/demo/wordpress-themes/liza/" class="share_facebook"><span class="fa fa-facebook"></span></a>
        <a target="_blank"
           href="https://twitter.com/intent/tweet?text=Home%20with%20Slideshow&#038;url=http://pixel-mafia.com/demo/wordpress-themes/liza/" class="share_twitter"><span class="fa fa-twitter"></span></a>
        <a target="_blank"
           href="https://plus.google.com/share?url=http://pixel-mafia.com/demo/wordpress-themes/liza/" class="share_gplus"><span class="fa fa-google-plus"></span></a>
        <a target="_blank"
           href="https://pinterest.com/pin/create/button/?url=http://pixel-mafia.com/demo/wordpress-themes/liza/" class="share_pinterest"><span class="fa fa-pinterest"></span></a>
    </div><!-- .liza_aside_menu_share -->
</div><!-- .liza_aside_menu_wrapper -->

<!-- Aside Header -->
<header class="liza_header logo_center menu_aside">
    <div class="liza_header_bg liza_blur_on_aside"></div>
    <div class="liza_header_wrapper">
        <div class="liza_header_left_part">
            <div class="liza_menu_block">
                <a class="liza_menu_toggler" href="javascript:void(0)">
						<span class="liza_menu_ico">
							<span class="liza_menu_ico_line1"></span>
							<span class="liza_menu_ico_line2"></span>
							<span class="liza_menu_ico_line3"></span>
						</span>Menu
                </a>
            </div>
        </div><!-- .liza_header_left_part -->

        <div class="liza_header_middle_part">
            <div class="liza_logo_wrapper liza_blur_on_aside">
                <a href="index.html" class="liza_image_logo liza_retina"><img src="img/logo.png" alt="" width="145" height="59" /></a>
            </div>
        </div><!-- .liza_header_middle_part -->

        <div class="liza_header_right_part">
            <div class="liza_social_icons liza_blur_on_aside">
                <ul class="liza_social_icons_list">
                    <li><a class='facebook' target='_blank' href='https://www.facebook.com/' title='Facebook'><i class='fa fa-facebook'></i></a></li>
                    <li><a class='twitter' target='_blank' href='https://twitter.com/' title='Twitter'><i class='fa fa-twitter'></i></a></li>
                    <li><a class='google-plus' target='_blank' href='https://plus.google.com/' title='Google+'><i class='fa fa-google-plus'></i></a></li>
                    <li><a class='instagram' target='_blank' href='https://www.instagram.com/' title='Instagram'><i class='fa fa-instagram'></i></a></li>
                    <li><a class='pinterest' target='_blank' href='https://www.pinterest.com/' title='Pinterest'><i class='fa fa-pinterest'></i></a></li>
                </ul>
            </div>
        </div><!-- .liza_header_right_part -->
        <div class="clear"></div>
    </div><!-- .liza_header_wrapper -->
</header><!-- .liza_header -->

<!-- Mobile Header -->
<header class="liza_mobile_header">
    <div class="liza_mobile_header_wrapper">
        <div class="liza_mobile_header_logo">
            <div class="liza_logo_wrapper liza_blur_on_aside">
                <a href="index.html" class="liza_image_logo liza_retina"><img src="img/logo.png" alt="" width="145" height="59" /></a>
            </div>
        </div><!-- .liza_mobile_header_logo -->
        <div class="liza_mobile_button_wrapper">
            <a class="liza_menu_toggler liza_menu_toggler_mobile" href="javascript:void(0)">
					<span class="liza_menu_ico">
						<span class="liza_menu_ico_line1"></span>
						<span class="liza_menu_ico_line2"></span>
						<span class="liza_menu_ico_line3"></span>
					</span>Menu
            </a>
        </div><!-- .liza_mobile_button_wrapper -->
    </div><!-- .liza_mobile_header_wrapper -->
    <div class="liza_mobile_menu_wrapper">
        <div class="liza_social_icons liza_social_icons_mobile liza_container">
            <ul class="liza_social_icons_list">
                <li><a class='facebook' target='_blank' href='https://www.facebook.com/' title='Facebook'><i class='fa fa-facebook'></i></a></li>
                <li><a class='twitter' target='_blank' href='https://twitter.com/' title='Twitter'><i class='fa fa-twitter'></i></a></li>
                <li><a class='google-plus' target='_blank' href='https://plus.google.com/' title='Google+'><i class='fa fa-google-plus'></i></a></li>
                <li><a class='instagram' target='_blank' href='https://www.instagram.com/' title='Instagram'><i class='fa fa-instagram'></i></a></li>
                <li><a class='pinterest' target='_blank' href='https://www.pinterest.com/' title='Pinterest'><i class='fa fa-pinterest'></i></a></li>
            </ul><!-- .liza_social_icons_list -->
        </div><!-- .liza_social_icons_mobile -->

        <nav class="liza_mobile_menu liza_container"></nav>
    </div><!-- .liza_mobile_menu_wrapper -->
</header><!-- .liza_mobile_header -->
