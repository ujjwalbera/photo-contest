<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <div class="pcoded-navigatio-lavel">Navigation</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="{{ route('admin-dashboard') }}">
                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                    <span class="pcoded-mtext">Dashboard</span>
                </a>
            </li>
        </ul>
        <div class="pcoded-navigatio-lavel">Entry Fee Management</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                    <span class="pcoded-mtext">Entry Fees</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ route('admin-packages') }}">
                            <span class="pcoded-mtext">Entry Fees</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ route('admin-package-add') }}">
                            <span class="pcoded-mtext">Add Entry Fee</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ route('admin-packages-inactive') }}">
                            <span class="pcoded-mtext">Inactive Entry Fees</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                    <span class="pcoded-mtext">Advance Components</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="draggable.htm">
                            <span class="pcoded-mtext">Draggable</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <div class="pcoded-navigatio-lavel">Competition Management</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                    <span class="pcoded-mtext">Competitions</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ route('admin-competitions') }}">
                            <span class="pcoded-mtext">Competitions</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ route('admin-competition-add') }}">
                            <span class="pcoded-mtext">Add Competition</span>
                        </a>
                    </li>
                    <li class=" ">
                        <a href="{{ route('admin-competition-inactive') }}">
                            <span class="pcoded-mtext">Old Competitions</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('admin-competition-participates') }}">
                    <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                    <span class="pcoded-mtext">Participates</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin-competition-categories') }}">
                    <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                    <span class="pcoded-mtext">Categories</span>
                </a>
            </li>

            <li>
                <a href="{{ route('admin-competition-cameras') }}">
                    <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                    <span class="pcoded-mtext">Cameras</span>
                </a>
            </li>
        </ul>
    </div>

    <div class="pcoded-navigatio-lavel">User Management</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="pcoded-hasmenu">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="feather icon-box"></i></span>
                    <span class="pcoded-mtext">Users</span>
                </a>
                <ul class="pcoded-submenu">
                    <li class=" ">
                        <a href="{{ route('admin-users') }}">
                            <span class="pcoded-mtext">Users</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('admin-competition-participates') }}">
                    <span class="pcoded-micon"><i class="feather icon-gitlab"></i></span>
                    <span class="pcoded-mtext">Participates</span>
                </a>
            </li>
        </ul>
    </div>
</nav>

