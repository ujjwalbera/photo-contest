﻿<!DOCTYPE html>
<html lang="en">

<head>
    <title>@yield('pageTitle') :: Photo Contest</title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="{!! URL::asset('assets/admin/adminty/images/favicon.ico') !!}" type="image/x-icon">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{!! URL::asset('assets/admin/adminty/vendor/bootstrap/css/bootstrap.min.css') !!}">
    <!-- radial chart.css -->
    <link rel="stylesheet" href="{!! URL::asset('assets/admin/adminty/pages/chart/radial/css/radial.css') !!}" type="text/css" media="all">

    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href=".{!! URL::asset('assets/admin/adminty/icon/themify-icons/themify-icons.css') !!}">
    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{!! URL::asset('assets/admin/adminty/icon/font-awesome/css/font-awesome.min.css') !!}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{!! URL::asset('assets/admin/adminty/icon/icofont/css/icofont.css') !!}">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="{!! URL::asset('assets/admin/adminty/icon/feather/css/feather.css') !!}">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{!! URL::asset('assets/admin/adminty/css/style.css') !!}">
    <link rel="stylesheet" type="text/css" href="{!! URL::asset('assets/admin/adminty/css/jquery.mCustomScrollbar.css') !!}">
</head>
<!-- Menu sidebar static layout -->

<body>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
            <div class="ring">
                <div class="frame"></div>
            </div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

    @include('layouts.admin.partials.header')


        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                @include('layouts.admin.partials.sidebar')
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">
                        <div class="main-body">
                            <div class="page-wrapper">
                                <div class="page-body">
                                    <div class="row">

                                        @yield('content')

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Required Jquery -->
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/vendor/jquery\js\jquery.min.js') !!}"></script>
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/vendor/jquery-ui\js\jquery-ui.min.js') !!}"></script>
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/vendor/popper.js\js\popper.min.js') !!}"></script>
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/vendor/bootstrap\js\bootstrap.min.js') !!}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/vendor/jquery-slimscroll\js\jquery.slimscroll.js') !!}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/vendor/modernizr\js\modernizr.js') !!}"></script>
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/vendor/modernizr\js\css-scrollbars.js') !!}"></script>
<!-- Chart js -->
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/vendor/chart.js\js\Chart.js') !!}"></script>
<!-- Google map js -->
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js') !!}"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?sensor=true"></script>
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/\pages\google-maps\gmaps.js') !!}"></script>
<!-- gauge js -->
<script src="{!! URL::asset('assets/admin/adminty/\pages\widget\gauge\gauge.min.js') !!}"></script>
<script src="{!! URL::asset('assets/admin/adminty/\pages\widget\amchart\amcharts.js') !!}"></script>
<script src="{!! URL::asset('assets/admin/adminty/\pages\widget\amchart\serial.js') !!}"></script>
<script src="{!! URL::asset('assets/admin/adminty/\pages\widget\amchart\gauge.js') !!}"></script>
<script src="{!! URL::asset('assets/admin/adminty/\pages\widget\amchart\pie.js') !!}"></script>
<script src="{!! URL::asset('assets/admin/adminty/\pages\widget\amchart\light.js') !!}"></script>
<!-- Custom js -->
<script src="{!! URL::asset('assets/admin/adminty/\js\pcoded.min.js') !!}"></script>
<script src="{!! URL::asset('assets/admin/adminty/\js\vartical-layout.min.js') !!}"></script>
<script src="{!! URL::asset('assets/admin/adminty/\js\jquery.mCustomScrollbar.concat.min.js') !!}"></script>
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/\pages\dashboard\crm-dashboard.min.js') !!}"></script>
<script type="text/javascript" src="{!! URL::asset('assets/admin/adminty/\js\script.js') !!}"></script>
</body>

</html>
