﻿@extends('layouts.customer.dashboard')
@section('pageTitle', 'Customer Participate in Competition')
@section('content')
    @if(count($competitions))
        <div class="col-md-8">
            <div class="card">
                <div class="flash-message">
                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                        @if(Session::has('alert-' . $msg))
                            <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                        @endif
                    @endforeach
                </div>
                <div class="card-header">
                    <strong>Participate</strong> in Competition
                </div>
                <form role="form" action="{{ route('customer-competition-doparticipate') }}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="card-body card-block">
                        @if(count($competitions) > 1)
                            <div class="form-group">
                                <label for="competition" class=" form-control-label">Competition</label>
                                <select name="competition" id="competition" class="form-control">
                                    <option value="">Select Competition</option>
                                    @foreach($competitions as $competition)
                                        <option value="{{ $competition->id }}" @if(old('competition') == $competition->id) selected @endif >{{$competition->name}}</option>
                                    @endforeach
                                </select>
                                @error('category') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                            </div>
                        @else
                            <input type="hidden" name="competition" value="{{ $competitions[0]->id }}">
                        @endif
                        <div class="form-group">
                            <label for="title" class=" form-control-label">Title</label>
                            <input type="text" id="title" name="title" placeholder="Image title..." class="form-control" value="{{ old('title') }}">
                            @error('title') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                        <div class="form-group">
                            <label for="description" class=" form-control-label">Description</label>
                            <textarea name="description" id="txtEditor" rows="3" placeholder="Content..." class="form-control description">{{ old('description')
                            }}</textarea>
                            @error('description') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                        <div class="form-group">
                            <label for="category" class=" form-control-label">Category</label>
                            <select name="category" id="category" class="form-control">
                                <option value="">Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if(old('category') == $category->id) selected @endif >{{$category->name}}</option>
                                @endforeach
                            </select>
                            @error('category') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                        <div class="form-group">
                            <label for="camera" class=" form-control-label">Select Camera</label>
                            <select name="camera" id="camera" class="form-control">
                                <option value="">Select Camera</option>
                                @foreach($cameras as $camera)
                                    <option value="{{ $camera->id }}" @if(old('camera') == $camera->id) selected @endif >{{$camera->name}}</option>
                                @endforeach
                            </select>
                            @error('camera') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                        <div class="form-group">
                            <label for="image" class=" form-control-label">Upload</label>
                            <input type="file" id="image" name="image" class="form-control-file" accept="image/x-png,image/jpg,image/jpeg">
                            @error('image') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary btn-sm">
                            <i class="fa fa-dot-circle-o"></i> Submit
                        </button>
                        <button type="reset" class="btn btn-danger btn-sm">
                            <i class="fa fa-ban"></i> Reset
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Participate Guide</strong>
                </div>
                <div class="card-body">
                    <div class="alert alert-primary" role="alert">
                        <ul class="vue-ordered" style="padding-left: 20px;">
                            <li>Uploaded image 0</li>
                            <li>Remaining image 4</li>
                        </ul>
                    </div>
                    <div class="typo-headers">
                        <h6>PREPARING YOUR IMAGES BEFORE UPLOADING:</h6>
                        <ol class="vue-ordered" style="padding-left: 20px;">
                            <li>Image Size must have a long dimension of at least <span class="badge badge-pill badge-success">1000 pixels</span> and less than
                                <span class="badge badge-pill badge-success">3000 pixels</span>.</li>
                            <li>Digital files should be <span class="badge badge-pill badge-success">72dpi</span>, sRGB or Adobe98.</li>
                            <li>Save file as <span class="badge badge-pill badge-success">JPG</span> with maximum quality.</li>
                            <li>File Size should be no more than <span class="badge badge-pill badge-success">4 Mb</span>.</li>
                            <li>Do not upload photos with watermark names, image frames, borders and logos.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <strong class="card-title">Need Help:</strong>
                </div>
                <div class="card-body">
                    <div class="vue-misc">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Address</h3>
                                <address class="mt-3">
                                    <strong>SJØNNA</strong>
                                    <br> Nezalezhnasti Ave, 13 - 28A
                                    <br> Minsk, Belarus, 220141
                                    <br> +375 29 319-53-98
                                    <br>
                                    <br>
                                    <b>Vasili Savitski</b>
                                    <br>
                                    <a href="mailto">hello@examplemail.com</a>
                                </address>
                            </div>
                            <div class="col-md-6">
                                <h3 class="mb-3">Ask us</h3>
                                <div class="card">
                                    <div class="card-body card-block">
                                        <div class="has-success form-group">
                                            <textarea name="textarea-input" id="textarea-input" rows="5" placeholder="Ask us..." class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary btn-sm">
                                            <i class="fa fa-dot-circle-o"></i> Submit
                                        </button>
                                        <button type="reset" class="btn btn-danger btn-sm">
                                            <i class="fa fa-ban"></i> Reset
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            /*$(document).ready(function() {
                $("#txtEditor").Editor();
                $('.Editor-editor').html({{ old('description') }});
            });*/

            $(document).ready(function(){
                var editor = $("#txtEditor").Editor();
                $("button:submit").click(function(){
                    $('.description').text($('#txtEditor').Editor("getText"));
                });
                $('.Editor-editor').append($('.description').text());
            });
        </script>
    @endif
@endsection
