﻿@extends('layouts.customer.dashboard')
@section('pageTitle', 'Customer Change Password')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>

            @if (Session::has('message'))
                <div class="alert alert-warning">{{ Session::get('message') }}</div>
            @endif
            <div class="card-header">
                <strong>Change Password</strong>
            </div>
            <form action="{{ route('customer-dochange-password') }}" method="post" enctype="multipart/form-data" class="form-horizontal">
                @csrf
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="current_password" class=" form-control-label">Current Password</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="password" id="current_password" name="current_password" placeholder="Current Password" class="form-control">
                            @error('current_password') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="new_password" class=" form-control-label">New Password</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="password" id="new_password" name="new_password" placeholder="New Password" class="form-control">
                            @error('new_password') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="new_confirm_password" class=" form-control-label">New Confirm Password</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="password" id="new_confirm_password" name="new_confirm_password" placeholder="New Confirm Password" class="form-control">
                            @error('new_confirm_password') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
