﻿@extends('layouts.customer.dashboard')
@section('pageTitle', 'Customer Subscriptions')
@section('content')
    <div class="col-md-12">
        <div class="table-responsive m-b-40">
            <table class="table table-borderless table-striped table-data3">
                <thead>
                <tr>
                    <th>#NO</th>
                    <th>Date</th>
                    <th>Transaction ID</th>
                    <th>Name</th>
                    <th class="text-right">Amount</th>
                    <th class="text-right">Image</th>
                </tr>
                </thead>
                <tbody>
                @if(count($packages))
                    @php $i = 0 @endphp
                    @foreach($packages as $package)
                    <tr>
                        <td>{{ $i++ }}</td>
                        <td>{{ date('d/m/Y H:i:s A', strtotime($package->created_at)) }}</td>
                        <td>{{$package->invoice_id}}</td>
                        <td>{{$package->package->name}}</td>
                        <td class="text-right"><i class="fas fa-{{$package->package->currency}}"></i> {{$package->package->price}}</td>
                        <td class="text-right">{{$package->package->image}} Images</td>
                    </tr>
                    @endforeach
                @else
                    <tr><td colspan="6" style="text-align: center; vertical-align: middle; color:red;"><b>No entry pass found!.</b></td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
