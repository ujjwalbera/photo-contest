@extends('layouts.customer.login')
@section('pageTitle', 'Customer Registration')
@section('content')
    <style type="text/css">
        .help-block {
            color: red;
        }
    </style>
<div class="login-form">
    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    @if (Session::has('message'))
        <div class="alert alert-warning">{{ Session::get('message') }}</div>
    @endif
    <form action="{{ route('customer-doregistration') }}" method="post">
        @csrf
        <div class="form-group">
            <label>Name</label>
            <input class="au-input au-input--full" type="text" name="name" placeholder="Full Name" value="{{ old('name') }}">
            @error('name') <span class="help-block"> {{ $message }} </span> @enderror
        </div>
        <div class="form-group">
            <label>Phone</label>
            <input class="au-input au-input--full" type="text" name="phone" placeholder="Phone" value="{{ old('phone') }}">
            @error('phone') <span class="help-block"> {{ $message }} </span> @enderror
        </div>
        <div class="form-group">
            <label>Email Address</label>
            <input class="au-input au-input--full" type="email" name="email" placeholder="Email" value="{{ old('email') }}">
            @error('email') <span class="help-block"> {{ $message }} </span> @enderror
        </div>
        <div class="form-group">
            <label>Password</label>
            <input class="au-input au-input--full" type="password" name="password" placeholder="Password" value="{{ old('password') }}">
            @error('password') <span class="help-block"> {{ $message }} </span> @enderror
        </div>
        <div class="form-group">
            <label>Confirm Password</label>
            <input class="au-input au-input--full" type="password" name="confirm_password" placeholder="Confirm Password" value="{{ old('confirm_password') }}">
            @error('confirm_password') <span class="help-block"> {{ $message }} </span> @enderror
        </div>
        <div class="login-checkbox">
            <label>
                <input type="checkbox" name="agree">Agree the terms and policy
            </label>

        </div>
        @error('agree') <span class="help-block"> {{ $message }} </span> @enderror
        <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">register</button>
        <div class="social-login-content">
            <div class="social-button">
                <button class="au-btn au-btn--block au-btn--blue m-b-20">register with facebook</button>
                <button class="au-btn au-btn--block au-btn--blue2">register with twitter</button>
            </div>
        </div>
    </form>
    <div class="register-link">
        <p>
            Already have account?
            <a href="{{route('customer-login')}}">Sign In</a>
        </p>
    </div>
</div>
@endsection
