﻿@extends('layouts.customer.dashboard')
@section('pageTitle', 'Customer Biography')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>
            <div class="card-header">
                <strong>Participate</strong> in Competition
            </div>
            <form role="form" action="{{ route('customer-dobiography-update') }}" method="post">
                @csrf
                <div class="card-body card-block">

                    <div class="form-group">
                        <label for="biography" class=" form-control-label">Biography</label>
                        <textarea name="biography" id="txtEditor" rows="3" placeholder="Content..." class="form-control biography">{{ old('biography')
                            }}</textarea>
                        @error('biography') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>

                    <div class="form-group">
                        <label for="facebook" class=" form-control-label">Facebook</label>
                        <input type="text" id="facebook" name="facebook" placeholder="Facebook profile url..." class="form-control" value="{{ old('facebook')
                        }}">
                        @error('facebook') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>

                    <div class="form-group">
                        <label for="instagram" class=" form-control-label">Instagram</label>
                        <input type="text" id="instagram" name="instagram" placeholder="Instagram profile url..." class="form-control" value="{{ old
                        ('instagram') }}">
                        @error('instagram') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>

                    <div class="form-group">
                        <label for="linkedin" class=" form-control-label">Linkedin</label>
                        <input type="text" id="linkedin" name="linkedin" placeholder="Linkedin profile url..." class="form-control" value="{{ old('linkedin') }}">
                        @error('linkedin') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>

                    <div class="form-group">
                        <label for="twitter" class=" form-control-label">Twitter</label>
                        <input type="text" id="twitter" name="twitter" placeholder="Twitter profile url..." class="form-control" value="{{ old('twitter') }}">
                        @error('twitter') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>
            </form>
        </div>
    </div>
    <script>
        /*$(document).ready(function() {
            $("#txtEditor").Editor();
            $('.Editor-editor').html({{ old('description') }});
            });*/

        $(document).ready(function(){
            var editor = $("#txtEditor").Editor();
            $("button:submit").click(function(){
                $('.biography').text($('#txtEditor').Editor("getText"));
            });
            $('.Editor-editor').append($('.biography').text());
        });
    </script>
@endsection
