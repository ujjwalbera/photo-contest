﻿@extends('layouts.customer.dashboard')
@section('pageTitle', 'Buy Subscription')
@section('content')
    @foreach($packages as $package)
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <strong class="card-title mb-3">{{$package->name}}</strong>
            </div>
            <div class="card-body">
                <div class="mx-auto d-block">
                    <h1 class="text-sm-center pb-2 display-4">{{$package->images}}</h1>
                    <h5 class="text-sm-center mt-2 mb-1">Images</h5>
                    <div class="location text-sm-center">
                        <i class="fa fa-{{$package->currency}}"></i> {{$package->price}}</div>
                </div>
                <hr>
                <div class="card-text text-sm-center">
                    <a href="{{ route('customer-payment-paypal', ['id' => $package->id]) }}" class="btn btn-success">Buy Now</a>
                </div>
            </div>
        </div>
    </div>
    @endforeach




    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <strong class="card-title">Terms and Condition :</strong>
            </div>
            <div class="card-body">
                <div class="typo-headers">
                    <ol class="vue-ordered" style="padding-left: 20px;">
                        <li>The amount for each Image is charged in USD </li>
                        <li>Once an Image fee has been paid, there are no refunds provided.</li>
                        <li>If you have been wrongly charged for any reason, we will be happy to credit the amount that is due, however once part of an images has been used no refunds are provided.</li>
                        <li>The Image is valid until the close of the competition year in which the Image is bought; whole or parts of an Image may not be carried forward into future years.</li>
                        <li>By entering an image as part of an Image the Photographer maintains Agreement with the Competition Rules.</li>
                    </ol>
                </div>
                <div class="vue-misc">
                    <h2 class="display-5 my-3">Still confuse</h2>
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Address</h3>
                            <address class="mt-3">
                                <strong>SJØNNA</strong>
                                <br> Nezalezhnasti Ave, 13 - 28A
                                <br> Minsk, Belarus, 220141
                                <br> +375 29 319-53-98
                                <br>
                                <br>
                                <b>Vasili Savitski</b>
                                <br>
                                <a href="mailto">hello@examplemail.com</a>
                            </address>
                        </div>
                        <div class="col-md-6">
                            <h3 class="mb-3">Ask us</h3>
                            <div class="card">
                                <div class="card-body card-block">
                                    <div class="has-success form-group">
                                        <textarea name="textarea-input" id="textarea-input" rows="5" placeholder="Ask us..." class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary btn-sm">
                                        <i class="fa fa-dot-circle-o"></i> Submit
                                    </button>
                                    <button type="reset" class="btn btn-danger btn-sm">
                                        <i class="fa fa-ban"></i> Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
