﻿@extends('layouts.customer.dashboard')
@section('pageTitle', 'Customer Participated Competition list')
@section('content')
    <div class="col-md-12">
        <div class="table-responsive m-b-40">
            <table class="table table-borderless table-striped table-data3">
                <thead>
                <tr>
                    <th>Title</th>
                    <th>Competition</th>
                    <th class="text-right">Category</th>
                    <th class="text-right">Camera</th>
                    <th class="text-right">Status</th>
                    <th class="text-right">Date</th>
                </tr>
                </thead>
                <tbody>
                @if(count($participates))
                    @foreach($participates as $participate)
                        <tr>
                            <td>{{$participate->title}}</td>
                            <td>{{$participate->competition->name}}</td>
                            <td class="text-right">{{$participate->category->name}}</td>
                            <td class="text-right">{{$participate->camera->name}}</td>
                            <td class="text-right">{{$participate->competition->participating}}</td>
                            <td class="text-right">{{ date('d/m/Y H:i:s A', strtotime($participate->created_at)) }}</td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="6" style="text-align: center; vertical-align: middle; color:red;"><b>No participates found!.</b></td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection
