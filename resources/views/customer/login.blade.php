@extends('layouts.customer.login')
@section('pageTitle', 'Customer Login')
@section('content')
<div class="login-form">

    <div class="flash-message">
        @foreach (['danger', 'warning', 'success', 'info'] as $msg)
            @if(Session::has('alert-' . $msg))
                <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
            @endif
        @endforeach
    </div>

    @if (Session::has('message'))
        <div class="alert alert-warning">{{ Session::get('message') }}</div>
    @endif
    <form action="{{ route('customer-dologin') }}" method="post">
        @csrf
        <div class="form-group">
            <label>Username</label>
            <input class="au-input au-input--full" type="username" name="username" placeholder="Email\Phone">
            @error('username') <span class="help-block"> {{ $message }} </span> @enderror
        </div>
        <div class="form-group">
            <label>Password</label>
            <input class="au-input au-input--full" type="password" name="password" placeholder="Password">
            @error('password') <span class="help-block"> {{ $message }} </span> @enderror
        </div>
        <div class="login-checkbox">
            <label>
                <input type="checkbox" name="remember">Remember Me
            </label>
            <label>
                <a href="#">Forgotten Password?</a>
            </label>
        </div>
        <button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">sign in</button>
        <div class="social-login-content">
            <div class="social-button">
                <button class="au-btn au-btn--block au-btn--blue m-b-20">sign in with facebook</button>
                <button class="au-btn au-btn--block au-btn--blue2">sign in with twitter</button>
            </div>
        </div>
    </form>
    <div class="register-link">
        <p>
            Don't you have account?
            <a href="{{route('customer-registration')}}">Sign Up Here</a>
        </p>
    </div>
</div>
@endsection
