﻿@extends('layouts.customer.dashboard')
@section('pageTitle', 'Customer Profile')
@section('content')
    <div class="col-lg-6">
        <div class="card">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('profile-alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>
            <div class="card-header">
                <strong>Profile</strong> Update
            </div>
            <form action="{{ route('customer-doprofile-update') }}" method="post" class="form-horizontal">
                @csrf
                <div class="card-body card-block">
                    <div class="row form-group">
                        <div class="col col-md-12">
                            <label for="name" class=" form-control-label">Name</label>
                        </div>
                        <div class="col-3">
                            <select name="salutation" id="salutation" class="form-control">
                                <option value="Mr." @if(old('name', $customer->salutation) == 'Mr.') selected @endif>Mr.</option>
                                <option value="Mrs." @if(old('name', $customer->salutation) == 'Mrs.') selected @endif>Mrs.</option>
                                <option value="Miss" @if(old('name', $customer->salutation) == 'Miss') selected @endif>Miss</option>
                                <option value="Dr." @if(old('name', $customer->salutation) == 'Dr.') selected @endif>Dr.</option>
                                <option value="Ms." @if(old('name', $customer->salutation) == 'Ms.') selected @endif>Ms.</option>
                                <option value="Prof." @if(old('name', $customer->salutation) == 'Prof.') selected @endif>Prof.</option>
                                <option value="Lady" @if(old('name', $customer->salutation) == 'Lady') selected @endif>Lady</option>
                                <option value="Sir" @if(old('name', $customer->salutation) == 'Sir') selected @endif>Sir</option>
                            </select>
                        </div>
                        <div class="col-9">
                            <input type="text" name="name" id="name" placeholder="Full name" class="form-control" value="{{ old('name', $customer->name) }}">
                        </div>
                        @error('name') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>
                    <div class="form-group">
                        <label for="profile_name" class="control-label mb-1">Profile name</label>
                        <input id="profile_name" name="profile_name" type="text" class="form-control" placeholder="Profile name" value="{{ old('profile_name', $customer->profile_name) }}">
                        @error('profile_name') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>

                    <div class="form-group">
                        <label for="profile_picture" class="control-label mb-1">Profile Picture</label>
                        <input id="profile_picture" name="profile_picture" type="file" class="form-control" placeholder="Profile picture">
                        @error('profile_picture') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>
                    <div class="form-group">
                        <label for="experience" class="control-label mb-1">Experience Level</label>
                        <select name="experience" id="experience" class="form-control">
                            <option value="Student Photographer" @if(old('experience', $customer->experience) == 'Student Photographer') selected @endif>Student Photographer</option>
                            <option value="Amateur Photographer" @if(old('experience', $customer->experience) == 'Amateur Photographer') selected @endif>Amateur Photographer</option>
                            <option value="Professional Photographer" @if(old('experience', $customer->experience) == 'Professional Photographer') selected @endif>Professional Photographer</option>
                        </select>
                        @error('experience') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>
                    <div class="form-group">
                        <label for="dob" class="control-label mb-1">Date of birth</label>
                        <input id="dob" name="dob" type="text" class="form-control" placeholder="Date of birth" value="{{ old('dob', $customer->dob) }}">
                        @error('dob') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card">
            <div class="flash-message">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has('contact-alert-' . $msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get('alert-' . $msg) }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                    @endif
                @endforeach
            </div>
            <div class="card-header">
                <strong>Contact info</strong> Update
            </div>
            <form action="{{ route('customer-docontact-update') }}" method="post" class="form-horizontal">
                @csrf
                <div class="card-body card-block">
                    <div class="form-group">
                        <label for="phone" class="control-label mb-1">Mobile Number</label>
                        <input id="phone" name="phone" type="text" class="form-control" placeholder="Mobile Number (without Country code)" value="{{ old('phone', $customer->phone) }}">
                        @error('phone') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>
                    <div class="form-group">
                        <label for="country" class="control-label mb-1">Country</label>
                        <select name="country" id="country" class="form-control">
                            <option>Select Country</option>
                            @if(count($countries))
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" @if(isset($customer->profile->country_id) && $customer->profile->country_id == $country->id) selected @endif>{{
                                    $country->name }}</option>
                                @endforeach
                            @endif
                        </select>
                        @error('country') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                            <label for="state" class="control-label mb-1">State</label>
                            <select name="state" id="state" class="form-control">
                                <option>Select State</option>
                                @if(count($states))
                                    @foreach ($states as $state)
                                        <option value="{{ $state->id }}" @if(isset($customer->profile->state_id) && $customer->profile->state_id == $state->id) selected @endif>{{
                                        $state->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('state') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                        <div class="col-6">
                            <label for="city" class="control-label mb-1">City</label>
                            <select name="city" id="city" class="form-control">
                                <option>Select City</option>
                                @if(count($cities))
                                    @foreach ($cities as $city)
                                        <option value="{{ $city->id }}" @if(isset($customer->profile->city_id) && $customer->profile->city_id == $city->id) selected @endif>{{ $city->name
                                        }}</option>
                                    @endforeach
                                @endif
                            </select>
                            @error('city') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address" class="control-label mb-1">Street Address</label>
                        <input id="address" name="address" type="text" class="form-control" placeholder="Street Address" value="{{ old('address', $customer->profile->address) }}">
                        @error('address') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>
                    <div class="form-group">
                        <label for="postcode" class="control-label mb-1">Postcode / ZIP</label>
                        <input id="postcode" name="postcode" type="text" class="form-control" placeholder="Postcode / ZIP" value="{{ old('postcode', $customer->profile->postcode) }}">
                        @error('postcode') <small class="form-text text-muted"> {{ $message }} </small> @enderror
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="fa fa-dot-circle-o"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-danger btn-sm">
                        <i class="fa fa-ban"></i> Reset
                    </button>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#country').change(function(){
                var countryID = $(this).val();
                if(countryID){
                    $.ajax({
                        type:"GET",
                        url:"{{url('api/get-state-list')}}/"+countryID,
                        success:function(res){
                            console.log(res);
                            if(res){
                                $("#state").empty();
                                $("#city").empty();
                                $("#city").append('<option>Select City</option>');
                                $("#state").append('<option>Select State</option>');
                                $.each(res,function(key,value){
                                    $("#state").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });

                            }else{
                                $("#state").empty();
                                $("#city").empty();
                            }
                        }
                    });
                }else{
                    $("#state").empty();
                    $("#city").empty();
                }
            });
            $('#state').on('change',function(){
                var stateID = $(this).val();
                if(stateID){
                    $.ajax({
                        type:"GET",
                        url:"{{url('api/get-city-list')}}/"+stateID,
                        success:function(res){
                            if(res){
                                $("#city").empty();
                                $("#city").append('<option>Select City</option>');
                                $.each(res,function(key,value){
                                    $("#city").append('<option value="'+value.id+'">'+value.name+'</option>');
                                });

                            }else{
                                $("#city").empty();
                            }
                        }
                    });
                }else{
                    $("#city").empty();
                }

            });
        });
    </script>
@endsection
