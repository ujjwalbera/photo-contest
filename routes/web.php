<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('layouts.users.home');
});*/

//Auth::routes(['verify' => true]);

Route::group(['middleware' => 'web'], function () {
    Route::get('/', 'PagesController@index')->name('home-index');



    Route::get('/customers', 'CustomersController@login')->name('customer-login');
    Route::post('/customers', 'CustomersController@doLogin')->name('customer-dologin');
    Route::get('/customers/registration', 'CustomersController@registration')->name('customer-registration');
    Route::post('/customers/registration', 'CustomersController@doRegistration')->name('customer-doregistration');
    Route::get('/customers/logout', 'CustomersController@doLogout')->name('customer-dologout');

    Route::get('email/verify/{id}', 'CustomersController@verify')->name('customer-verify');
    Route::get('email/resend', 'CustomersController@resend')->name('customer-verification-resend');

    Route::get('/customers/dashboard', 'CustomersController@dashboard')->name('customer-dashboard');
    Route::get('/customers/packages', 'PackagesController@customer_package')->name('customer-package');
    Route::get('/customers/buy/package', 'PackagesController@package_list')->name('customer-package-buy');

    Route::get('/customers/pay/package/{id}', 'PaymentsController@payWithpaypal')->name('customer-payment-paypal');
    Route::get('/customers/paid/package/status', 'PaymentsController@getPaymentStatus')->name('customer-payment-status');

    Route::get('/customers/competitions', 'CompetitionsController@customerParticipateList')->name('customer-competition-participate-lists');
    Route::get('/customers/competition/participate', 'CompetitionsController@customerParticipate')->name('customer-competition-participate');
    Route::post('/customers/competition/participate', 'CompetitionsController@doCustomerParticipate')->name('customer-competition-doparticipate');

    Route::get('/customers/update/profile', 'CustomersController@update_profile')->name('customer-profile');
    Route::post('/customers/update/profile', 'CustomersController@doUpdateProfile')->name('customer-doprofile-update');
    Route::post('/customers/update/contact', 'CustomersController@doUpdateContact')->name('customer-docontact-update');

    Route::get('/customers/update/biography', 'CustomersController@update_biography')->name('customer-biography');
    Route::post('/customers/update/biography', 'CustomersController@doPpdateBiography')->name('customer-dobiography-update');
    Route::get('/customers/change-password', 'CustomersController@change_password')->name('customer-change-password');
    Route::post('/customers/change-password', 'CustomersController@doChangePassword')->name('customer-dochange-password');


    Route::get('api/get-state-list/{id}','AddressController@getStateList');
    Route::get('api/get-city-list/{id}','AddressController@getCityList');







    Route::get('/admins', 'AdminsController@login')->name('admin-login');
    Route::post('/admins', 'AdminsController@doLogin')->name('admin-dologin');
    Route::get('/admins/registration', 'AdminsController@registration')->name('admin-registration');
    Route::post('/admins/registration', 'AdminsController@doRegistration')->name('admin-doregistration');
    Route::get('/admins/logout', 'AdminsController@doLogout')->name('admin-dologout');
    Route::get('/admins/dashboard', 'AdminsController@dashboard')->name('admin-dashboard');


    Route::get('/admins/packages', 'PackagesController@admin_packages')->name('admin-packages');
    Route::get('/admins/package/add', 'PackagesController@admin_package_add')->name('admin-package-add');
    Route::post('/admins/package/add', 'PackagesController@admin_package_doadd')->name('admin-package-doadd');
    Route::get('/admins/package/edit/{$id}', 'PackagesController@admin_package_edit')->name('admin-package-edit');
    Route::post('/admins/package/edit/{$id}', 'PackagesController@admin_package_doedit')->name('admin-package-doedit');
    Route::get('/admins/inactive-packages', 'PackagesController@admin_packages_inactive_list')->name('admin-packages-inactive');
    Route::post('/admins/package/delete/{$id}', 'PackagesController@admin_package_delete')->name('admin-package-delete');


    Route::get('/admins/competitions', 'CompetitionsController@admin_competitions')->name('admin-competitions');
    Route::get('/admins/competition/add', 'CompetitionsController@admin_competition_add')->name('admin-competition-add');
    Route::post('/admins/competition/add', 'CompetitionsController@admin_competition_doadd')->name('admin-competition-doadd');
    Route::get('/admins/competition/edit/{$id}', 'CompetitionsController@admin_competition_edit')->name('admin-competition-edit');
    Route::post('/admins/competition/edit/{$id}', 'CompetitionsController@admin_competition_doedit')->name('admin-competition-doedit');
    Route::get('/admins/inactive-competitions', 'CompetitionsController@admin_competitions_inactive_list')->name('admin-competition-inactive');
    Route::post('/admins/competition/delete/{$id}', 'CompetitionsController@admin_competition_delete')->name('admin-competition-delete');

    Route::get('/admins/competition/participates', 'CompetitionsController@admin_competition_participates')->name('admin-competition-participates');

});


