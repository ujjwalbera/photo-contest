<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerAward extends Model
{
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;


    /**
     * Indicates key type.
     *
     * @var string
     */
    //public $keyType = 'string';

    protected $table = 'customer_awards';
}
