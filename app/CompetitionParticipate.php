<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetitionParticipate extends Model
{
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = true;


    /**
     * Indicates key type.
     *
     * @var string
     */
    //public $keyType = 'string';

    protected $table = 'competition_participates';

    public function competition(){
        return $this->belongsTo('App\Competition');
    }

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function camera(){
        return $this->belongsTo('App\Camera');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }
}
