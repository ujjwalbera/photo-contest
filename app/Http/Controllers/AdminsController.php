<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use DB;
use Mail;

class AdminsController extends Controller
{
    protected $table = 'admins';

    /**
     * @return void
     */
    public function login() {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin-dashboard');
        }
        return view('admin.login');
    }

    /**
     * @desc customer login action
     * @param  Request  $request
     * @return void
     */
    public function doLogin(Request $request)  {
        //dd($request);
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $data = $request->validate($rules);
        $login_type = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL ) ? 'email' : 'phone';
        $request->merge([ $login_type => $request->input('username') ]);
        if (Auth::guard('admin')->attempt($request->only($login_type, 'password'))) {
            //return redirect()->intended($this->redirectPath());
            $request->session()->flash('alert-success', 'Admin was successful login!');
            return redirect()->route('admin-dashboard');
        } else {
            $request->session()->flash('alert-warning', 'Invalid Credentials , Please try again.');
            return redirect()->route('admin-login');
        }
    }

    /**
     * @return void
     */
    public function dashboard() {
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin-login');
        }
        return view('admin.dashboard');
    }
}
