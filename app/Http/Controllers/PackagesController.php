<?php

namespace App\Http\Controllers;

use App\Package;
use App\CustomerPackage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackagesController extends Controller
{

    public function __construct()
    {
    }

    public function customer_package(Request $request){
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        $currentCustomer = Auth::guard('customer')->user()->id;
        $packages = CustomerPackage::with('package')->where('customer_id', $currentCustomer)->get();
        return view('customer.packages', compact('packages'));
    }

    public function package_list(Request $request){
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        $packages = Package::where('status', true)->get();
        return view('customer.package_list', compact('packages'));
    }

    public function admin_packages(Request $request){
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin-login');
        }
        $packages = Package::where('status', true)->get();
        return view('admin.packages.list', compact('packages'));
    }

    public function admin_packages_inactive_list(Request $request){
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin-login');
        }
        $packages = Package::where('status', false)->get();
        return view('admin.packages.inactive_list', compact('packages'));
    }
}
