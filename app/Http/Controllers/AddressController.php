<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Requests;
use DB;

use App\State;
use App\City;


class AddressController extends Controller
{

    public function getStateList(Request $request, $id)
    {
        $states = State::select('id', 'name')->where('country_id', $id)->where('status', true)->get();
        return response()->json($states);
    }
    public function getCityList(Request $request, $id)
    {
        $cities = City::select('id', 'name')->where('state_id', $id)->where('status', true)->get();
        return response()->json($cities);
    }
}
