<?php

namespace App\Http\Controllers;


use App\Subscription;
use App\CompetitionParticipate;
use App\Competition;
use App\Category;
use App\Camera;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class CompetitionsController extends Controller
{

    public function __construct() {
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
    }

    public function customerParticipateList(Request $request){
        $customerID = Auth::guard('customer')->user()->id;
        $participates = CompetitionParticipate::with(['competition', 'category'])->where('customer_id', $customerID)->where('status', true)->get();
        //dd($participates);
        //$competitions = Competition::whereDate('close_date', '>=', date('Y-m-d H:i:s'))->where('status', true)->where('participating', 'running')->get();
        return view('customer.participate_list', compact('participates'));
    }

    public function customerParticipate(Request $request){
        $customerID = Auth::guard('customer')->user()->id;
        //$participates = CompetitionParticipate::where('customer_id', $customerID)->where('status', true)->get();
        $competitions = Competition::whereDate('close_date', '>=', date('Y-m-d H:i:s'))->where('status', true)->where('participating', 'running')->get();
        $categories = Category::where('status', true)->get();
        $cameras = Camera::where('status', true)->get();
        return view('customer.participate', compact('competitions', 'categories', 'cameras'));
    }


    public function doCustomerParticipate(Request $request){
        //dd($request);
        $rules = [
            'title' => 'required|regex:/^[A-Za-z0-9 ]+$/',
            'description' => 'required',
            'category' => 'required',
            'camera' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,JPEG,PNG,JPG|max:4096|dimensions:min_width=1000,max_width=3000,min_height=1000,max_height=3000',
        ];

        $customMessages = [
            'required' => 'The :attribute field is required.',
            'regex' => 'Invalid format attribute :attribute',
            'image.image' => 'Only image allowed.',
            'image.mimes' => 'Only jpeg,png,jpg image allowed.',
            'image.max' => 'File Size should be no more than 4 Mb.',
            'image.dimensions' => 'Image size must have at least 1000 pixels and less than 3000 pixels.',
        ];
        //$validator = Validator::make($request->all(), $rules, $customMessages);
        $data = $request->validate($rules, $customMessages);
        try {
            if($request->file('image')){
                //dd($request->input);
                $upload = $request->file('image');
                $imageName = time().'.'.$upload->getClientOriginalExtension();
                $destinationPath = public_path('/images/competition/participates');
                try {
                    $upload->move($destinationPath, $imageName);
                    $participate = new CompetitionParticipate();
                    $participate->title = $request->input( 'title' );
                    $participate->description = $request->input( 'description' );
                    $participate->category_id = $request->input( 'category' );
                    $participate->camera_id = $request->input( 'camera' );
                    $participate->customer_id = Auth::guard('customer')->user()->id;
                    $competitions = Competition::whereDate('close_date', '>=', date('Y-m-d H:i:s'))->where('status', true)->where('participating', 'running')->get();
                    if(count($competitions) > 1) {
                        $participate->competition_id = $request->input( 'competition' );
                    } else {
                        $participate->competition_id = $competitions[0]->id;
                    }
                    $participate->status = true;
                    $participate->upload = 'images/competition/participates/' .$imageName;
                    $participate->save();
                    $request->session()->flash('alert-success', 'You successfully submit image in competition!');
                    return redirect()->route('customer-competition-participate');
                } catch(\Exception $e) {
                    $request->session()->flash('alert-error', $e->getMessage());
                    return redirect()->route('customer-competition-participate');
                }
            }else{
                $request->session()->flash('alert-success', 'You successfully submit image in competition!');
                return redirect()->route('customer-competition-participate');
            }
        } catch(\Exception $e) {
            $request->session()->flash('alert-error', $e->getMessage());
            return redirect()->route('customer-competition-participate');
        }
    }

    public function admin_competitions(Request $request){
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin-login');
        }
        $competitions = Competition::where('status', true)->get();
        return view('admin.competitions.list', compact('competitions'));
    }

    public function admin_competitions_inactive_list(Request $request){
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin-login');
        }
        $competitions = Competition::where('status', false)->get();
        return view('admin.competitions.inactive_list', compact('competitions'));
    }

    public function admin_competition_participates(Request $request){
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin-login');
        }
        $participates = CompetitionParticipate::with(['competition', 'category', 'camera', 'customer'])->where('status', true)->get();
        //dd($participates);
        return view('admin.competitions.participates_list', compact('participates'));
    }
}
