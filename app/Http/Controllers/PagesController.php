<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Banner;


class PagesController extends Controller
{



    public function __construct(){
        //parent::__construct();
    }



    public function index() {
        $banners = Banner::where('status', true)->where('placeholder', 'home_banner')->get();
        $categories = Category::where('status', true)->orderBy('position', 'ASC')->get();
        return view('site.home.index', compact('banners', 'categories'));
    }
}
