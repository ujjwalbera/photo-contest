<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Package;
use App\Payment as PaymentModel;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Payment;
use PayPal\Rest\ApiContext;
use PayPal\Api\Transaction;

use Session;
use Input;

class PaymentsController extends Controller {

    public $_api_context = null;


    public function __construct() {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        //dd($paypal_conf);
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
                $paypal_conf['client_id'],
                $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithpaypal(Request $request, $id)
    {
        $package = Package::where('id', $id)->first();
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName($package->name)->setCurrency($package->currency_code)->setQuantity(1)->setPrice($package->price); /** unit price **/
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency($package->currency_code)->setTotal($package->price);
        $transaction = new Transaction();
        $transaction->setAmount($amount)->setItemList($item_list)->setDescription($package->details);
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(route('customer-payment-status'))->setCancelUrl(route('customer-payment-status'));
        $payment = new Payment();
        $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)->setTransactions(array($transaction));
        /** dd($payment->create($this->_api_context));exit; **/

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            $request->session()->flash('alert-error', 'Some error occur, sorry for inconvenient!');
            return redirect()->back();
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            $request->session()->flash('alert-error', 'Some error occur, sorry for inconvenient!');
            return redirect()->back();
        }
        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            return redirect()->away($redirect_url);
        }
        $request->session()->flash('alert-error', 'Some error occur, sorry for inconvenient!');
        return redirect()->back();
    }

    public function getPaymentStatus(Request $request) {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty($request->get('PayerID')) || empty($request->get('token'))) {

            $request->session()->flash('alert-error', 'Some error occur, Payment failed!');
            return redirect()->back();

        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId($request->get('PayerID'));

        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        if ($result->getState() == 'approved') {
            $request->session()->flash('alert-success', 'Payment successfully done!');
            return redirect()->back();
        }
        $request->session()->flash('alert-error', 'Some error occur, Payment failed!');
        return redirect()->back();

    }

    public function payment(Request $request){
        $subscription=Subscription::find($request->id);
        return view('payment',compact('subscription'));
    }


    public function paymentInfo(Request $request){
        if($request->tx){
            if($payment=PaymentModel::where('transaction_id',$request->tx)->first()){
                $payment_id=$payment->id;
            }else{
                $payment=new PaymentModel;
                $payment->subscription_id=$request->item_number;
                $payment->transaction_id=$request->tx;
                $payment->currency_code=$request->cc;
                $payment->payment_status=$request->st;
                $payment->save();
                $payment_id=$payment->id;
            }
            return 'Pyament has been done and your payment id is : '.$payment_id;
        }else{
            return 'Payment has failed';
        }
    }
}
