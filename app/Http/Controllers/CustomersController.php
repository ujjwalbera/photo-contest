<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Mail;

//use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Verified;

use App\Customer;
use App\Country;
use App\State;
use App\City;
use App\CustomerProfile;

class CustomersController extends Controller
{
    //use VerifiesEmails;

    protected $table = 'customers';

    /**
     * @return void
     */
    public function login() {
        if (Auth::guard('customer')->check()) {
            return redirect()->route('customer-dashboard');
        }
        return view('customer.login');
    }

    /**
     * @desc customer login action
     * @param  Request  $request
     * @return void
     */
    public function doLogin(Request $request)  {
        //dd($request);
        $rules = [
            'username' => 'required',
            'password' => 'required'
        ];
        $data = $request->validate($rules);
        $login_type = filter_var($request->input('username'), FILTER_VALIDATE_EMAIL ) ? 'email' : 'phone';
        $request->merge([ $login_type => $request->input('username') ]);
        if (Auth::guard('customer')->attempt($request->only($login_type, 'password'))) {
            //return redirect()->intended($this->redirectPath());
            $request->session()->flash('alert-success', 'Customer was successful login!');
            return redirect()->route('customer-dashboard');
        } else {
            $request->session()->flash('alert-warning', 'Invalid Credentials , Please try again.');
            return redirect()->route('customer-login');
        }
    }

    /**
     * @return void
     */
    public function doLogout(Request $request)
    {
        Auth::guard('customer')->logout();
        $request->session()->flash('alert-success', 'Customer was successfully logged out!');
        return redirect()->route('customer-login');
    }

    /**
     * @return void
     */
    public function registration()
    {
        if (Auth::guard('customer')->check()) {
            return redirect()->route('customer-dashboard');
        }
        return view('customer.register');
    }



    /**
     * @return void
     */
    public function doRegistration(Request $request)
    {
        $rules = [
            'name' => 'required|regex:/^[A-Za-z ]+$/',
            'phone' => 'nullable|numeric|regex:/[0-9]{10}/|unique:' . $this->table,
            'email' => 'required|email|unique:' . $this->table,
            'password' => 'required',
            'confirm_password' => 'same:password',
            'agree' => 'required'
        ];

        $customMessages = [
            'required' => 'The :attribute field is required.',
            'regex' => 'Invalid format attribute :attribute',
            'name.required' => 'Your name is required',
            'name.regex' => 'Given name is not a valid format',
            'email.required' => 'Your email is required',
            'email.email' => 'Given email is not a valid format',
            'email.unique' => 'This email already registered.',
            'phone.numeric' => 'Put your phone number with out country code',
            'phone.unique' => 'Your phone number already used. Try reset',
            'password.required' => 'Password is required',
            'agree.required' => 'Your must agree the terms and policy',
        ];

        //$validator = Validator::make($request->all(), $rules, $customMessages);
        $data = $request->validate($rules, $customMessages);
        $customer = new Customer();

        $customer->salutation = '';
        $customer->name = $request->input( 'name' );
        $customer->profile_name = $request->input( 'name' );
        $customer->phone = $request->input( 'phone' );
        $customer->email = $request->input( 'email' );
        $customer->password = Hash::make( $request->input( 'password' ) );
        $customer->remember_token = $request->input( '_token' );
        $customer->profile_picture = '';
        $customer->dob = date('Y-m-d');
        $customer->experience = '';
        $customer->save();

        //$this->registration_confirmation($request, $user->id);
        Mail::to($customer->email)->send();
        $request->session()->flash('alert-success', 'Customer was successful registration!');
        return redirect()->back();
    }


    /**
     * @return void
     */
    public function dashboard() {
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        return view('customer.dashboard');
    }

    /**
     * @return void
     */
    public function update_profile() {
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        $cities = [];
        $states = [];
        $customerID = Auth::guard('customer')->user()->id;
        $customer = Customer::with('profile', 'awards')->where('id', $customerID)->first();
        $countries = Country::select('id', 'name')->where('status', true)->get();
        if(isset($customer->profile->state_id)) {
            $states = State::select('id', 'name')->where('status', true)->where('country_id', $customer->profile->country_id)->get();
        }
        if(isset($customer->profile->city_id)) {
            $cities = City::select('id', 'name')->where('status', true)->where('state_id', $customer->profile->state_id)->get();
        }
        return view('customer.profile', compact('customer', 'countries', 'states', 'cities'));
    }

    /**
     * @return void
     */
    public function doUpdateProfile(Request $request) {
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        $rules = [
            'name' => 'required|regex:/^[A-Za-z ]+$/',
            'profile_name' => 'required',
            'profile_picture' => 'required|mimes:jpeg,png,jpg,JPEG,PNG,JPG|max:1024|dimensions:min_width=200,max_width=400,min_height=200,max_height=400',
            'dob' => 'required'
        ];

        $customMessages = [
            'required' => 'The :attribute field is required.',
            'regex' => 'Invalid format attribute :attribute',
            'name.required' => 'Your name is required',
            'name.regex' => 'Given name is not a valid format',
            'dob.required' => 'Your Date of birth is required',
            'profile_picture.image' => 'Only image allowed.',
            'profile_picture.mimes' => 'Only jpeg,png,jpg image allowed.',
            'profile_picture.max' => 'File Size should be no more than 1 Mb.',
            'profile_picture.dimensions' => 'Image size must have at least 200 pixels and less than 400 pixels.',
        ];

        //$validator = Validator::make($request->all(), $rules, $customMessages);
        $data = $request->validate($rules, $customMessages);
        $customer = new Customer();
        $customer->salutation = $request->input( 'salutation' );
        $customer->name = $request->input( 'name' );
        $customer->profile_name = $request->input( 'profile_name' );
        $customer->profile_picture = $request->input( 'profile_picture' );
        $customer->experience = $request->input( 'experience' );
        $customer->dob = $request->input( 'dob' );
        $customer->save();

        //$this->registration_confirmation($request, $user->id);
        $request->session()->flash('profile-alert-success', 'Customer Profile was successful Updated!');
        return redirect()->back();
    }


    /**
    * Mark the authenticated user’s email address as verified.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function verify(Request $request) {
        $userID = $request['id'];
        $user = Customer::findOrFail($userID);
        $date = date('Y-m-d g:i:s');
        $user->email_verified_at = $date; // to enable the “email_verified_at field of that user be a current time stamp by mimicing the must verify email feature
        $user->save();
        return response()->json('Email verified!');
    }
    /**
    * Resend the email verification notification.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
        return response()->json('User already have verified email!', 422);
        // return redirect($this->redirectPath());
        }
        $request->user()->sendEmailVerificationNotification();
        return response()->json('The notification has been resubmitted');
        // return back()->with(‘resent’, true);
    }




    /**
     * @return void
     */
    public function doUpdateContact(Request $request) {
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        return view('customer.profile');
    }

    /**
     * @return void
     */
    public function update_biography() {
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        $customerID = Auth::guard('customer')->user()->id;
        $customer = Customer::with('profile')->where('id', $customerID)->first();
        return view('customer.biography', compact('customer'));
    }

    /**
     * @return void
     */
    public function doPpdateBiography(Request $request) {
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        $customerID = Auth::guard('customer')->user()->id;
        $customer = Customer::with('profile')->where('id', $customerID)->first();
        if($customer->has('profile')) {
            $profile = CustomerProfile::where('customer_id', $customerID)->first();
        } else {
            $profile = new CustomerProfile();
        }

        $profile->biography = $request->input( 'biography' );
        $profile->facebook = $request->input( 'facebook' );
        $profile->instagram = $request->input( 'instagram' );
        $profile->linkedin = $request->input( 'linkedin' );
        $profile->twitter = $request->input( 'twitter' );
        $profile->save();

        //$this->registration_confirmation($request, $user->id);
        $request->session()->flash('biography-alert-success', 'Customer biography was successful Updated!');
        return redirect()->back();
    }

    /**
     * @return void
     */
    public function change_password() {
        if (!Auth::guard('customer')->check()) {
            return redirect()->route('customer-login');
        }
        return view('customer.change_password');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function doChangePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => 'required',
            'new_confirm_password' => 'same:new_password',
        ]);

        Customer::find(Auth::guard('customer')->user()->id)->update(['password'=> Hash::make($request->new_password)]);
        $request->session()->flash('alert-success', 'Customer was successful change password!');
        return redirect()->back();
    }
}
