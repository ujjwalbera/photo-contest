<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{

    public function customersubscription(){
        return $this->hasOne('App\CustomerPackage');
    }
}
